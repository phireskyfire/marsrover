# README #


### What is this repository for? ###

* This application executes a text file that moves the Mars Rover safely in a specific zone.
* Version 1.0

To use this program run it in Visual Studio by pressing F5. This will launch a console application. The console application has single input which is the path of the text file to execute.
The required content of the text file is described below:

### Text file format ###

The expected file format is:
8 8

1 2 E

MMLMRMMRRMML 

The first line in the text file describes the area to cover in pre-defined zone on Mars. This is a Cartesian coordinate.
"8 8" would mean that the rover should only explore in that zone on the Y axis between 0 and 8 and on the X axis between 0 and 8.
The app will ignore commands that fall outside of these coordinates.

The second line is the initial direction that rover starts in:
The first character is the starting position on the X axis. 
The second character is the starting location on the Y axis.
The third character is the initial direction:

	N = North
	
	E = East
	
	W = West
	
	S= South
	
Note: The characters should be capitalised and delimited by a single space.

The output of the application will be the final destination of the Mars Rover after executing the text file. 
It will give the value of the X and Y coordinates and the final direction it will be facing in.

The following command are undestood by the Rover:

• M - Move one space forward in the direction it is facing

• R - rotate 90 degrees to the right

• L - rotate 90 degrees to the left 
	
### How do I get set up? ###


* Open the solution file next45.phiresky.rover.app.sln using Visual Studio 2017 or later
* Run the code by hitting f5

### Running unit tests ###
  Open the Test Explorer in Visual Studio -> Click on "Run All Test"
  
 The following will be run:
  
  - FileSerialize:This will test the serialization of the txt file to c# objects. The test project includes various example text files. 
  
  - StartTest This will serialize and the execute the included txt file: testCommand1.txt The test passes when the known output of:
  	X:3
	
	Y:3
	
	Direction: South
	
	Is the result

### Design Decisions ###
The application is designed to be as simple as possible to debug and to understand. 
Considerable effort was made for every possible value that is supplied in the text file to be strongly typed.

Full documentation available in the "next45.phiresky.rover.app/Documentation" folder

The text file is serialized using the following classes:

TextFile

	- ZoneString (String)
	
	- InitialDirectionString (String)
	
	- CommandString (String)

RoverInput

	- RoverZone (Zone)
	
	- RoverLocation (Location)
	
	- RoverCommands (List<Commands>)
	
Zone

	- X (int)
	
	- Y (int)
	
Location

	- X (int)
	
	- Y (int)
	
	- Direction (Enum Direction: North East West South)
	
The application has been devided into the following helper classes:

	- FileHandler
	
	- RoverMover
	
FileHandler

	Public Methods:
	
		- SerializeFile (Takes a file path as a parameter) Converts a text file into a RoverInput object
		
		- ParseTextFile (Takes a TextFile object as a parameter) Coverts a TextFile object to a RoverInput object
		
	Private Methods:
	
		- GetZoneFromString (Takes a Zone String as a paramter) This converts a zone string to a Zone object
		
		- GetLocationFromString (Takes a Location String as a parameter) This converts a Location string to a Location object
		
		- GetCommandFromString (Takes a Command String as a paramter) This converts a Command string to a generic list of Command objects
		
RoverMover

	Public Methods:
	
		- Start(Takes a file path as a paramter) Creates a RoverInput object. Returns a Location object with the final destination of the Rover
		
	Private Methods:
	
		- ExecuteCommand(Takes a RoverInput object as paramter) Returns a Location object with the Rover's final destination
		
		- Program (Starting point)
			Public Methods:
			
		- Main Lauches the console app
		
	Private Methods:
	
		- GetRoverInput. A recursive method that get the console input to run the appliction.
	


	
