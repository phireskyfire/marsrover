﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app
{
    public enum Commands { M, R, L }
    public class RoverInput
    {
        public Zone RoverZone { get; set; }
        public Location RoverLocation { get; set; }
        public List<Commands> RoverCommands { get; set; }
    }
}
