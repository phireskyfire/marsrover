﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app
{
    public class Zone
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
