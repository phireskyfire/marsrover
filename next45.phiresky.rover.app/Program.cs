﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app
{
    class Program
    {
        static void Main(string[] args)
        {
            GetRoverInput();
        }

        private static void GetRoverInput() {
            try
            {
                Console.Write("Enter the full file path for the Rover command text file:");
                String Path = Console.ReadLine();
                RoverMover RM = new RoverMover();
                Location FinalLocation = RM.Start(Path);
                Console.WriteLine(String.Format("X Postion:{0}", FinalLocation.X));
                Console.WriteLine(String.Format("Y Postion:{0}", FinalLocation.Y));
                Console.WriteLine(String.Format("Direction:{0}", FinalLocation.Direction.ToString()));
                GetRoverInput();
            }
            catch (Exception ex)
            {

                Console.WriteLine(String.Format("Error:{0}",ex.Message));
                GetRoverInput();
            }

        }
    }
}
