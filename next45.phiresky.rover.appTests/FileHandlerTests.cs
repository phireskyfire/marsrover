﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using next45.phiresky.rover.app;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app.Tests
{
    [TestClass()]
    public class FileHandlerTests
    {
    

        [TestMethod()]
        public void SerializeFileTest()
        {
            FileHandler FH = new FileHandler();
            try
            {

                DirectoryInfo TestFileDirectory =  new DirectoryInfo("../../TestFiles");
                RoverInput RI = FH.SerializeFile(String.Format(@"{0}\testCommand1.txt",TestFileDirectory.FullName));
                RoverInput RI2 = FH.SerializeFile(String.Format(@"{0}\testCommand2.txt", TestFileDirectory.FullName));

            }
            catch (Exception ex)
            {
                Assert.Fail();
            }

           
        }
    }
}