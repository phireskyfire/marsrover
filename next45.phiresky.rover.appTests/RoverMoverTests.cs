﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using next45.phiresky.rover.app;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app.Tests
{
    [TestClass()]
    public class RoverMoverTests
    {
        [TestMethod()]
        public void StartTest()
        {
            RoverMover RM = new RoverMover();
            DirectoryInfo TestFileDirectory = new DirectoryInfo("../../TestFiles");
            String TestFile =String.Format(@"{0}\testCommand1.txt", TestFileDirectory.FullName);


            Location LastPosition = RM.Start(TestFile);
            Assert.AreEqual(LastPosition.X, 3);
            Assert.AreEqual(LastPosition.Y, 3);
            Assert.AreEqual(LastPosition.Direction, Direction.South);

            Location LastPosition2 = RM.Start(@"C:\rover\testCommand2.txt");
    

        }

    }
}
