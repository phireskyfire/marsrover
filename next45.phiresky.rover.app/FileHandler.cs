﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app
{
    public class FileHandler
    {
        /// <summary>
        /// Serializes a text file into a RoverInput Object
        /// </summary>
        /// <param name="Path"></param>
        /// <returns>A rover Input Object</returns>
        public RoverInput SerializeFile(String Path)
        {

            int counter = 0;
            string line;

            RoverInput Result = new RoverInput();

            Boolean FileExists = System.IO.File.Exists(Path);
            // Read the file and store it in a Textfile Object
            if (FileExists)
            {
                System.IO.StreamReader file = new System.IO.StreamReader(Path);
                TextFile TextFileParse = new TextFile();
                while ((line = file.ReadLine()) != null)
                {
                    switch (counter)
                    {
                        case 0:
                            TextFileParse.ZoneString = line;
                            break;
                        case 1:
                            TextFileParse.InitialDirectionString = line;
                            break;
                        case 2:
                            TextFileParse.CommandString = line;
                            break;

                    }
                    counter++;
                }
                Result = ParseTextFile(TextFileParse);
                file.Close();
                return Result;
            }
            else {
                throw (new Exception("The file does not exist"));
            }
            
            // Suspend the screen.
           
        }

        /// <summary>
        /// Creates A Zone object from a string
        /// </summary>
        /// <param name="ZoneString"></param>
        /// <returns>Zone Object</returns>
        private Zone GetZoneFromString(String ZoneString)
        {

            try
            {
                String[] SplitString = ZoneString.Split(' ');
                var Zone = new Zone();
                Zone.X = Convert.ToInt32(SplitString[0]);
                Zone.Y = Convert.ToInt32(SplitString[1]);
                return Zone;
            }
            catch (Exception)
            {

                throw(new Exception("The zone line in the text file is invalid!"));
            }

        }
        /// <summary>
        /// Creates Location Object from a String
        /// </summary>
        /// <param name="LocationString"></param>
        /// <returns>Location Object</returns>
        private Location GetLocationFromString(String LocationString)
        {

            try
            {
                String[] SplitString = LocationString.Split(' ');

                Location MarsLocation = new Location();
                MarsLocation.X = Convert.ToInt32(SplitString[0]);
                MarsLocation.Y = Convert.ToInt32(SplitString[1]);

                switch (SplitString[2])
                {
                    case "N":
                        MarsLocation.Direction = Direction.North;
                        break;
                    case "E":
                        MarsLocation.Direction = Direction.East;
                        break;
                    case "W":
                        MarsLocation.Direction = Direction.West;
                        break;
                    case "S":
                        MarsLocation.Direction = Direction.North;
                        break;
                }
                return MarsLocation;
            }
            catch (Exception)
            {

                throw(new Exception("The current location line in the text file is incorrect!"));
            }
            
         

           

        }

        /// <summary>
        /// Creates a generic list of Commands from a String
        /// </summary>
        /// <param name="CommandString"></param>
        /// <returns>List<Commands></returns>
        private List<Commands> GetCommandFromString(String CommandString)
        {
            try
            {
                List<Commands> MarsCommands = new List<Commands>();

                foreach (var command in CommandString)
                {
                    Commands Command;
                    Enum.TryParse(command.ToString(), out Command);
                    MarsCommands.Add(Command);

                }
                return MarsCommands;
            }
            catch (Exception)
            {

                throw(new Exception("The command line in the text file is invalid!"));
            }
        }
        /// <summary>
        /// Creates a RoverInput Object From a TextFile Object
        /// </summary>
        /// <param name="FileStrings"></param>
        /// <returns>RoverInput Object</returns>
        public RoverInput ParseTextFile(TextFile FileStrings)
        {
           
                RoverInput Result = new RoverInput();

                Result.RoverZone = GetZoneFromString(FileStrings.ZoneString);

                Result.RoverLocation = GetLocationFromString(FileStrings.InitialDirectionString);

                Result.RoverCommands = GetCommandFromString(FileStrings.CommandString);

                return Result;
           
        }
    }
}
