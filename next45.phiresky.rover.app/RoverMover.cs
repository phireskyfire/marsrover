﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app
{
    public class RoverMover
    {
        /// <summary>
        ///  This is the main entry point for the app.
        /// </summary>
        /// <param name="Path">The File path for the string for example c:\temp\command.txt</param>
        /// <returns>The Rover's location after the commands has been executed</returns>
        public Location Start(String Path)
        {

            FileHandler FH = new FileHandler();
            RoverInput Input = FH.SerializeFile(Path);
            return ExecuteCommand(Input);

        }
        /// <summary>
        /// Execute the command
        /// </summary>
        /// <param name="Input">Accepts a RoverInput Object</param>
        /// <returns>The final Location of the Rover as a Location Object</returns>
        public Location ExecuteCommand(RoverInput Input)
        {

            try
            {
                Location CurrentLocation = Input.RoverLocation;
                //Processes the rover command
                foreach (var command in Input.RoverCommands)
                {

                    switch (command)
                    {
                        //Move
                        case Commands.M:
                            switch (CurrentLocation.Direction)
                            {
                                case Direction.North:
                                    if (CurrentLocation.Y + 1 <= Input.RoverZone.Y)
                                    {
                                        CurrentLocation.Y++;
                                    }
                                    break;
                                case Direction.East:

                                    if (CurrentLocation.X + 1 <= Input.RoverZone.X)
                                    {
                                        CurrentLocation.X++;
                                    }
                                    break;
                                case Direction.West:
                                    if (CurrentLocation.X - 1 >= 0)
                                    {
                                        CurrentLocation.X--;
                                    }
                                    break;
                                case Direction.South:
                                    if (CurrentLocation.Y - 1 <= 0)
                                    {
                                        CurrentLocation.Y--;
                                    }
                                    break;
                            }

                            break;
                        //Right
                        case Commands.R:

                            switch (CurrentLocation.Direction)
                            {
                                case Direction.North:
                                    CurrentLocation.Direction = Direction.East;
                                    break;
                                case Direction.East:
                                    CurrentLocation.Direction = Direction.South;
                                    break;
                                case Direction.West:
                                    CurrentLocation.Direction = Direction.North;
                                    break;
                                case Direction.South:
                                    CurrentLocation.Direction = Direction.West;
                                    break;
                            }

                            break;
                        //Left
                        case Commands.L:
                            switch (CurrentLocation.Direction)
                            {
                                case Direction.North:
                                    CurrentLocation.Direction = Direction.West;
                                    break;
                                case Direction.East:
                                    CurrentLocation.Direction = Direction.North;
                                    break;
                                case Direction.West:
                                    CurrentLocation.Direction = Direction.South;
                                    break;
                                case Direction.South:
                                    CurrentLocation.Direction = Direction.East;
                                    break;
                            }
                            break;

                    }

                }

                return CurrentLocation;
            }
            catch (Exception)
            {

                throw(new Exception("There was a problem executing the command line on the text file!"));
            }

        }

    }
}
