﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next45.phiresky.rover.app
{
    public class TextFile
    {
        public String ZoneString { get; set; }
        public String  InitialDirectionString { get; set; }
        public String CommandString { get; set; }
    }
}
